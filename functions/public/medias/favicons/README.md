# Your Favicon Package

This package was generated with [RealFaviconGenerator](https://realfavicongenerator.net/) [v0.16](https://realfavicongenerator.net/change_log#v0.16)

## Install instructions

To install this package:

Extract this package in <code>&lt;web site&gt;/public/medias/</code>. If your site is <code>http://www.example.com</code>, you should be able to access a file named <code>http://www.example.com/medias/favicon.ico</code>.

Insert the following code in the `head` section of your pages:

    <link rel="apple-touch-icon" sizes="180x180" href="/medias/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/medias/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/medias/favicon-16x16.png">
    <link rel="manifest" href="/medias/site.webmanifest">
    <link rel="mask-icon" href="/medias/safari-pinned-tab.svg" color="#364f6b">
    <link rel="shortcut icon" href="/medias/favicon.ico">
    <meta name="msapplication-TileColor" content="#364f6b">
    <meta name="msapplication-config" content="/medias/browserconfig.xml">
    <meta name="theme-color" content="#364f6b">

*Optional* - Check your favicon with the [favicon checker](https://realfavicongenerator.net/favicon_checker)