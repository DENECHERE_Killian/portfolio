{
	"titre": "Morpion",
	"carte": {
		"image": "morpion.webp",
		"url": "morpion",
		"redirection": "",
		"description": {
			"fr": "Robot joueur de morpion",
			"en": "Tic-tac-toe player robot"
		}
	},
	"details": {
		"dates": {
			"debut": "10/2021",
			"fin": "01/2022"
		},
		"contexte": {
			"personnel": false,
			"scolaire": true,
			"nombreDePersonnes": 3
		}
	},
	"parties": [
		{
			"titre": {
				"fr": "Sujet",
				"en": "Subject"
			},
			"description": {
				"fr": "Ce projet a été initié quelques années avant que nous le reprenions en groupe. Nous devions rerendre le projet, le corriger et l'améliorer si possible.",
				"en": "This project was initiated a few years before we took it over as a group. We had to re-render the project, correct it and improve it if possible."
			},
			"mode": "horizontal",
			"sections": [
				{
					"titre": {
						"fr": "But du projet",
						"en": "Project goal"
					},
					"contenu": {
						"fr": "Pour permettre au bras robotisé de jouer au morpion, celui-ci est équipé d'une caméra et d'une pince pneumatique. La caméra permet de détecter les pièces de jeu et la pince permet de les saisir pour les déplacer.",
						"en": "To allow the robotic arm to play tic-tac-toe, it is equipped with a camera and a pneumatic clamp. The camera detects the game pieces and the clamp allows them to be grasped to move them."
					},
					"mode": "imageEtTexte",
					"donnees": {
						"titre": {},
						"image": "morpion-side.webp",
						"texte": {
							"style": "display: flex; align-items: center;",
							"fr": "Pour permettre au bras robotisé de jouer au morpion, celui-ci est équipé d'une caméra et d'une pince pneumatique. La caméra permet de détecter les pièces de jeu et la pince permet de les saisir pour les déplacer.",
							"en": "To allow the robotic arm to play tic-tac-toe, it is equipped with a camera and a pneumatic clamp. The camera detects the game pieces and the clamp allows them to be grasped to move them."
						},
						"texteEnPremier": true
					}
				},
				{
					"titre": {
						"fr": "Fonctionnement global",
						"en": "Global operation"
					},
					"contenu": {
						"fr": "Les images issues de la caméra sont analysées par le système pour repérer les pièces de jeu. Une fois les positions de chaque pièces obtenues, une algorithme (minmax) se charge de calculer le mouvement lui permettant de maximiser ses chances de victoire. Le bras robotisé est ensuite commandé pour saisir les pièces et les déplacer sur le plateau de jeu.",
						"en": "The images from the camera are analyzed by the system to locate the game pieces. Once the positions of each piece have been obtained, an algorithm (minmax) takes care of calculating the movement allowing it to maximize its chances of victory. The robotic arm is then controlled to grasp the pieces and move them on the game board."
					},
					"mode": "imageEtTexte",
					"donnees": {
						"titre": {},
						"image": "morpion-move.mp4",
						"texte": {
							"style": "display: flex; align-items: center;",
							"fr": "Le bras est équipé d'une pince pneumatique lui permettant de saisir des disques imprimés en 3D (les pièces de jeu) du plateau placé en face de lui. Tout à tour, le robot et le joueur vont placer leurs pions pour tenter de gagner la partie.",
							"en": "The arm is equipped with a pneumatic clamp that allows it to grasp 3D printed discs (game pieces) from the board placed in front of it. In turn, the robot and the player will place their pawns to try to win the game."
						},
						"texteEnPremier": true
					}
				}
			]
		},
		{
			"titre": {
				"fr": "Réalisation",
				"en": "Realization"
			},
			"description": {
				"fr": "Nous avons repris le projet en groupe de 3 personnes. Nous avons dû le corriger et l'améliorer. Nous avons donc corrigé les bugs et amélioré l'interface graphique. Nous avons également ajouté un mode de jeu contre l'ordinateur.",
				"en": "We took over the project as a group of 3 people. We had to correct and improve it. So we fixed the bugs and improved the graphical interface. We also added a game mode against the computer."
			},
			"mode": "horizontal",
			"sections": [
				{
					"mode": "vertical",
					"sections": [
						{
							"titre": {
								"fr": "Refonte de l'interface avec QT Creator",
								"en": "Redesign of the interface with QT Creator"
							},
							"mode": "imageEtTexte",
							"donnees": {
								"titre": {},
								"image": "morpion-qt.webp",
								"texte": {
									"style": "display: flex; align-items: center;",
									"fr": "Avec l'équipe, nous avons totalement refait l'interface de l'application en utilisant QT Creator.",
									"en": "With the team, we completely redesigned the application interface using QT Creator."
								},
								"texteEnPremier": true
							}
						},
						{
							"titre": {
								"fr": "Carrousel des images de l'interface avant et après refonte",
								"en": "Carousel of interface images before and after redesign"
							},
							"mode": "carousel",
							"listeImages": [
								{
									"lien": "morpion-ui-before.webp",
									"commentaire": {
										"fr": "Menu principal avant refonte",
										"en": "Menu principal before redesign"
									}
								},
								{
									"lien": "morpion-ui-after.webp",
									"commentaire": {
										"fr": "Menu principal après refonte",
										"en": "Menu principal after redesign"
									}
								},
								{
									"lien": "morpion-board.webp",
									"commentaire": {
										"fr": "Plateau de jeu avant refonte",
										"en": "Game board before redesign"
									}
								}
							]
						}
					]
				},
				{
					"titre": {
						"fr": "Amélioration de la logique du robot",
						"en": "Improvement of the robot's logic"
					},
					"mode": "texte",
					"texte": {
						"fr": "Nous avons retravaillé la structure de tous les fichiers du programme pour simplifier et corriger le code déjà existant. Une fois ce travail fait, il fallait aussi imaginer des moyens pour permettre à l'utilisateur de calibrer le robot avant de lancer une partie. En effet, puisque le robot se base sur des images du plateau, la détection des images peut varier en fonction des conditions lumineuses de la pièces et autres paramètres liés à l'environnement.",
						"en": "We reworked the structure of all the program files to simplify and correct the existing code. Once this work was done, we also had to think of ways to allow the user to calibrate the robot before starting a game. Indeed, since the robot is based on images of the board, the detection of images can vary depending on the lighting conditions of the room and other environment-related parameters."
					}
				}
			]
		}
	],
	"conception": {
		"mode": "conception",
		"langages": [
			{
				"abrege": "py",
				"utilite": {
					"fr": "Python est le langage de programmation principal du projet. C'est lui qui intéragi avec l'utilisateur, gère les calculs et commande l'API du bras robot.",
					"en": "It is the main programming language of the project. It is the one who interacts with the user, manages the calculations and commands the robot arm API."
				}
			}
		],
		"librairies": [
			{
				"abrege": "qt",
				"utilite": {
					"fr": "QT est une librairie qui permet de créer des interfaces graphiques.",
					"en": "QT is a library that allows you to create graphical interfaces."
				}
			}
		],
		"ressources": [
			{
				"abrege": "dobot",
				"utilite": {
					"fr": "Dobot Magician est un bras robot qui permet de déplacer des objets.",
					"en": "Dobot Magician is a robot arm that can move objects."
				}
			}
		]
	},
	"liensExternes": [
	]
}