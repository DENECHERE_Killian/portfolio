// noinspection JSUnresolvedFunction
const aside        = document.querySelector("aside");
let affichagePetit = window.innerWidth < 1000;

// Gestion des téléphones avec des tailles d'écran lus petites
window.addEventListener('resize', verifierTailleAffichage);
// Gestion de l'appuye sur la touche echap pour cacher le menu
document.addEventListener("keydown", (e) => {
	// Si le fondenuMobile n'est pas affiché, c'est que le menu n'est pas développé
	if (e.key !== "Escape" || document.querySelector(".fondMenuMobile") === null)
		return;

	cacherMenuApresAffichageMobile();
});

if (affichagePetit) {
	animerChangementAffichageTaille("petit");
	gererBoutons("petit");
}

// Affectation des événements
document.querySelectorAll('.menuAffichagePetit').forEach((elem) => elem.addEventListener('click', switchTaille));

// Permet d'afficher une partie précise parmis celles présentes sur une page
function afficherPartie(numero) {
	const partieCourante = document.querySelector(`#main-${indexPartieCourante}`);
	const partieSuivante = document.querySelector(`#main-${numero}`);

	if (partieCourante)
		partieCourante.style.display = "none";
	else
		document.querySelector("body > p")?.remove();

	changerImageH3(numero);
	if (partieSuivante)
		partieSuivante.style.display = "";
	window.history.replaceState("", "", window.location.pathname.substring(0, -1) + numero + window.location.search);

	if (affichagePetit)
		cacherMenuApresAffichageMobile();
}

// Permet d'afficher l'image de flèche sur les menus placés à gauche lorsqu'on clique dessus
function changerImageH3(numero) {
	if (numero === indexPartieCourante)
		return;

	const ul = document.querySelector("body > aside > ul");
	let img  = document.createElement("img");

	img.setAttribute("src", "/medias/icons/project/fleche_droite.svg");
	img.setAttribute("alt", lang === 'fr' ? "Indication de la partie actuellement affichée dans le main" : "Indication of the currently displayed part in the hand");
	ul.children[numero].prepend(img);
	ul.children[numero].style.marginRight = "10%";

	const ulPartieCourante             = ul.children[indexPartieCourante];
	ulPartieCourante.style.marginRight = "";
	if (ulPartieCourante.querySelector("img"))
		ulPartieCourante.querySelector("img").remove();

	indexPartieCourante = numero;
}

// Permet de lancer les changements d'afichage lorsque la page change de taille.
function verifierTailleAffichage() {
	if (!affichagePetit && window.innerWidth <= 1000) { // L'affichage est passé de grand à petit.
		affichagePetit = true;
		cacherMenuApresAffichageMobile();
		gererBoutons("petit");

	} else if (affichagePetit && window.innerWidth > 1000) { // L'affichage est passé de petit à grand.
		cacherMenuApresAffichageMobile();
		affichagePetit = false;
		animerChangementAffichageTaille("grand");
		gererBoutons("grand");
	}
}

// Cache ou affiche le menu de gauche en fonction du sens passé argument : petit = caché, grand = affiché.
function animerChangementAffichageTaille(sens) {
	anime({
		targets: aside,
		before: () => {
			aside.style.position = (sens === "petit") ? "fixed" : "";
		},
		translateX: () => {
			return (sens === "petit") ? `-${aside.clientWidth}20` : 0;
		},
		duration: 0
	});
}

// Permet d'afficher le bouton pour afficher le menu de gauche lorsqu'il est caché sur chacun des mains (toutes les parties).
function gererBoutons(sens) {
	document.querySelectorAll(".menuAffichagePetit").forEach((item) => {
		item.style.display = (sens === "petit") ? "flex" : "none";
	});
}

/**
 * Permet d'afficher le menu caché sur l'affichage mobile
 *
 * @return {void}
 */
function switchTaille() {
	animerChangementAffichageTaille("grand");
	document.documentElement.style.overflow = "hidden"; // Pour cacher la barre de défilemment vertical
	aside.style.position                    = "fixed";
	aside.style.width                       = "70vw";
	aside.style.zIndex                      = "3";

	const div = document.createElement("div");
	div.classList.add("fondMenuMobile");
	document.body.appendChild(div);
	div.addEventListener('click', cacherMenuApresAffichageMobile);
	div.addEventListener('wheel', (e) => {
		if (e.cancelable) e.preventDefault();
	});
	div.addEventListener('touchmove', (e) => {
		if (e.cancelable) e.preventDefault();
	});
}

/**
 * Permet de cacher le menu caché sur l'affichage mobile
 *
 * @return {void}
 */
function cacherMenuApresAffichageMobile() {
	document.documentElement.style.overflow = "auto"; // Pour réafficher la barre de défilemment vertical
	aside.style.position                    = "";
	aside.style.width                       = "";
	aside.style.zIndex                      = "";
	animerChangementAffichageTaille("petit");
	const fond = document.querySelector(".fondMenuMobile");
	if (fond)
		fond.remove();
}

// Gère les swipes à droite pour afficher le menu de gauche lorsque caché (pour mobile)
document.addEventListener('swiped', function (e) {
	if ((e.detail.xStart > window.innerWidth / 10) || ((e.detail.xEnd - e.detail.xStart) < window.innerWidth / 3))
		return; // Il faut swipe depuis le bord gauche (sur 1/10 de la largeur) et sur le tirer de la largeur de l'écran pour activer la fonction.

	if (e.detail.dir === "right")
		switchTaille();

	/*if (e.detail.dir == "left")
	 cacherMenuApresAffichageMobile();*/
});
