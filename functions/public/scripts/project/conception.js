// Changement de la partie droite au click sur une ressource/langage/librairie
function afficherRessource(id) {
  for (const section of document.querySelectorAll("article > section")) {
    if (section.attributes.id.value === `section-${id}`) {
      section.style.display = "";
    }else{
      section.style.display = "none";
    }
  }
}

// Repère si un langage/librairie/autre est à afficher lors du chargement
if (window.location.hash) {
  const div = document.querySelector(window.location.hash).children[0];
  div.click();
}

afficherPartie(indexPartieCourante); // Afficher le contenu de l'onglet sélectionné par défaut.
// Il faut la laisser après la vérif placée juste au-dessus sinon le hash disparait.
