// noinspection JSUnresolvedFunction

/*!
 * ImgPreviewer v1.0.4
 * https://github.com/yue1123/img-previewer
 *
 * Copyright 2021-present dh
 * Released under the MIT license
 *
 * Date: 2021-05-05T13:48:45.677Z
 *
 * Modified by Killian D for this project
 * https://gitlab.com/DENECHERE_Killian/portfolio
 */

(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
      typeof define === 'function' && define.amd ? define(factory) :
          (global = typeof globalThis !== 'undefined' ? globalThis : global || self, global.ImgPreviewer = factory());
}(this, (function() { 'use strict';

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor))
      throw new TypeError("Cannot call a class as a function");
  }

  function _defineProperties(target, props) {
    for (let i = 0; i < props.length; i++) {
      const descriptor       = props[i];
      descriptor.enumerable  = descriptor.enumerable || false;
      descriptor.configurable = true;

      if ("value" in descriptor)
        descriptor.writable = true;

      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps)
      _defineProperties(Constructor.prototype, protoProps);

    if (staticProps)
      _defineProperties(Constructor, staticProps);

    return Constructor;
  }

  /**
   * 防抖函数
   * @export {Funcion}
   * @param  {Function} fn 回调函数
   * @param  {Number} delay 防抖时间
   * @returns {Function}
   */
  function debounce(fn, delay) {
    let timer = null;
    return (arg) => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        fn(arg);
        clearTimeout(timer);
      }, delay);
    };
  }

  /**
   * 阻止默认事件
   * @param {Object} e
   */
  function preventDefault(e) {
    if (e.cancelable)
      e.preventDefault();
  }

  /**
   * {
   *   start:10,
   *   end:10,
   *   step:1,
   *   style:'font-size'
   *   template:'$px'
   * }
   */
  function runAnimation(el, options, callback) {
    const requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
    const cancelAnimationFrame  = window.cancelAnimationFrame  || window.mozCancelAnimationFrame;
    const step  = options.step;
    const end   = options.end || 0;
    let start   = options.start || 0;
    let playing = null;

    function running() {
      if (step > 0 && start < end || step < 0 && start > end) {
        start += step;
        el.style[options.style] = options.template.replace('$', start);
        playing = requestAnimationFrame(running);
      } else {
        callback && callback();
        cancelAnimationFrame(playing);
      }
    }

    running();
  }

  // 获取元素位置
  function isElementInViewport(el) {
    const viewPortHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    const offsetTop      = el.offsetTop;
    const offsetHeight   = el.offsetHeight;
    const scrollTop      = document.documentElement.scrollTop;
    const top            = offsetTop - scrollTop;
    return offsetHeight + offsetTop > scrollTop && top <= viewPortHeight + 100;
  }

  // 计算放大倍数
  function getElementRect(el) {
    return el.getBoundingClientRect();
  }

  // 设置图片样式
  function calcScaleNums(width, height, ratio) {
    let scaleX = windowWidth * ratio / width;
    let scaleY = windowHeight * ratio / height;

    return scaleX > scaleY ? scaleY : scaleX;
  }

  function setImageBaseStyle(el, width, height, left, top) {
    if (width > height)
      el.style.cssText = `width:${width}px;position:fixed; top:${top}px; left:${left}px;`;
    else
      el.style.cssText = `height:${height}px;position:fixed; top:${top}px; left:${left}px;`;
  }

  function taggleScrollBar(flag) {
    _BODY.style.overflow = flag ? 'auto' : 'hidden';
  }

  let _BODY, windowHeight, windowWidth;
  let historyInfo       = null;
  let currentImageScale = 0;
  let numberOfInstances = 0; // The number of previewer instanciated on this page.
  let _DEFAULT          = {
    ratio: 0.8,
    zoom: {
      min: 0.5,
      max: 5,
      step: 0.1
    },
    opacity: 0.6,
    scrollbar: true
  };

  return function () {
    // update image list
    function ImagePreviewer(selector) {
      const options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      _classCallCheck(this, ImagePreviewer);

      this.previewContainer = null;
      this.imageElements    = [];
      this.selector         = selector;
      this.imageEl          = null;
      this.options          = options;
      this.index            = 0;

      options.zoom          = Object.assign({}, _DEFAULT.zoom, options.zoom || {});
      this.config           = Object.assign({}, _DEFAULT, options);

      if (typeof selector != 'object')
        throw new Error('selector need to be a DOM object');

      if (!selector)
        throw new Error('selector is invalid');

      this.init();
    }

    _createClass(ImagePreviewer, [{
      key: "update",
      value: function update() {
        const _this = this;

        this.imageElements = this.selector.querySelectorAll("img:not(#preview-image)");
        this.imageElements.forEach((item, index) => {
          item.onclick = null;
          item.onclick = function (e) {
            _this.handleOpen(e, index);

            _this.taggleModel(true);

            _this.updateIndex(index);
          };
        });
      } // 绑定事件

    }, {
      key: "bindEvent",
      value: function bindEvent() {
        const _this2 = this;
        const mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(window.navigator.userAgent);
        const touchstart = mobile ? 'touchstart' : 'mousedown';
        const touchend = mobile ? 'touchend' : 'mouseup';
        const touchmove = mobile ? 'touchmove' : 'mousemove';

        this.imageElements.forEach((item, index) => {
          item.onclick = (e) => {
            document.onkeydown = (e) => {
              this.manageKeyPressEvents(e, this);
            };
            _this2.handleOpen(e, index);
            _this2.taggleModel(true);
            _this2.updateIndex(index);
          };
        }); // 点击关闭

        document.querySelector(`#image-preview-container-${this.instanceNum} .close`).addEventListener('click', () => {
          _this2.handleClose();
        }); // 重置样式

        document.querySelector(`#image-preview-container-${this.instanceNum} .reset`).addEventListener('click', () => {
          _this2.handleReset();
        }); // 上一张

        document.querySelector(`#image-preview-container-${this.instanceNum} .prev`)?.addEventListener('click', () => {
          _this2.prev();
        }); // 下一张

        document.querySelector(`#image-preview-container-${this.instanceNum} .next`)?.addEventListener('click', () => {
          _this2.next();
        }); // 蒙版点击关闭

        this.previewContainer.querySelector(".image-container").addEventListener('wheel', preventDefault);
        this.previewContainer.querySelector(".image-container").addEventListener('touchmove', preventDefault);
        // this.previewContainer.addEventListener('wheel', (e) => { e.preventDefault(); e.stopPropagation(); });
        // this.previewContainer.addEventListener('touchmove', (e) => { e.preventDefault(); e.stopPropagation(); });
        this.previewContainer.addEventListener('click', (e) => {
          if (e.target.classList[0] === 'image-container') {
            _this2.handleClose();
          }
        }); // 拖动图片

        this.imageEl.addEventListener(touchstart, (e) => {
          if (e.cancelable)
            e.preventDefault();
          let diffX, diffY;

          if (e.type === 'mousedown') { // Mouse
            // console.log("start mouse");
            diffX = e.clientX - this.imageEl.offsetLeft;
            diffY = e.clientY - this.imageEl.offsetTop;
          } else { // Touch
            // console.log("start touch");
            diffX = e.changedTouches[0].clientX - this.imageEl.offsetLeft;
            diffY = e.changedTouches[0].clientY - this.imageEl.offsetTop;
          }

          this.imageEl.classList.add('moving');
          this.imageEl.style.transition = "inherit"; // Pour avoir un déplacement sans latence

          this.imageEl[`on${touchmove}`] = function (e) {
            if (e.cancelable)
              e.preventDefault();
            if (e.type === 'mousemove') { // Mouse
              // console.log("moove mouse");
              this.style.left = `${e.clientX - diffX}px`;
              this.style.top = `${e.clientY - diffY}px`;
            } else { // Touch
              // console.log("moove touch");
              this.style.left = `${e.changedTouches[0].clientX - diffX}px`;
              this.style.top = `${e.changedTouches[0].clientY - diffY}px`;
            }
          };

          function moovStopped() {
            this.classList.remove('moving');
            this.style.transition = "";
          }

          this.imageEl[`on${touchend}`] = moovStopped;
          this.imageEl.onmouseout = moovStopped;
        });

        this.imageEl.addEventListener('wheel', function (e) {
          if (e.cancelable)
            e.preventDefault();
          this.style.transition = "inherit"; // Pour avoir un zoom sans latence
          const _this2$config$zoom = _this2.config.zoom,
              min = _this2$config$zoom.min,
              max = _this2$config$zoom.max,
              step = _this2$config$zoom.step;

          if (e.deltaY < 0 && currentImageScale < max)
            currentImageScale += step;
          else if (e.deltaY > 0 && currentImageScale > min)
            currentImageScale -= step;

          this.style.setProperty('--scale', currentImageScale.toFixed(2));
        }, true); // 旋转图片

        document.querySelector(`#image-preview-container-${this.instanceNum} .rotate-right`).addEventListener('click', () => {
          _this2.handelRotateRight();
        });
        document.querySelector(`#image-preview-container-${this.instanceNum} .rotate-left`).addEventListener('click', () => {
          _this2.handelRotateLeft();
        }); // 阻止默认事件

        document.ondragstart = preventDefault;
        document.ondragend = preventDefault; // 窗口大小改变
        window.onresize = debounce.bind(null, () => {
          _this2.handleClose();

          windowWidth = window.innerWidth;
          windowHeight = window.innerHeight;
        }, 100)();
      } // 重置

    }, {
      key: "handleReset",
      value: function handleReset() {
        this.imageEl.style.top = `${historyInfo.startY}px`;
        this.imageEl.style.left = `${historyInfo.startX}px`;
        this.imageEl.style.setProperty('--rotate', `0deg`);
        this.imageEl.style.setProperty('--scale', historyInfo.scale);
        historyInfo.rotate = 0;
      } // 打开预览

    }, {
      key: "manageKeyPressEvents",
      value: function manageKeyPressEvents(e, previewContainer) {
        if (previewContainer.previewContainer.style.display === "none")
          return; // S'il n'y a pas de preview d'affichée, on ne fait rien

        // console.log(e.key);
        if (e.key === "Escape")
          return previewContainer.handleClose();

        if (e.key === "ArrowLeft")
          return previewContainer.prev();

        if (e.key === "ArrowRight")
          return previewContainer.next();

        if (e.key === "ArrowUp") {
          if (e.cancelable)
            e.preventDefault();
          return previewContainer.handelRotateLeft();
        }

        if (e.key === "ArrowDown") {
          if (e.cancelable)
            e.preventDefault();
          return previewContainer.handelRotateRight();
        }
      }

    }, {
      key: "taggleModel",
      value: function taggleModel(flag) {
        this.previewContainer.style.display = flag ? 'block' : 'none';
      }

    }, {
      key: "handleOpen",
      value: function handleOpen(e, index) {
        const ratio = this.config.ratio;
        const imageElements = this.imageElements;
        const _e$target = e.target,
            width = _e$target.width,
            height = _e$target.height;
        const startX = e.clientX - e.offsetX;
        const startY = e.clientY - e.offsetY + 1;
        currentImageScale = calcScaleNums(width, height, ratio);
        historyInfo = {
          startX: startX,
          startY: startY,
          endX: windowWidth / 2 - width / 2 - startX,
          endY: windowHeight / 2 - height / 2 - startY,
          scale: currentImageScale,
          rotate: 0
        };

        this.imageEl.src = imageElements[index].src;
        setImageBaseStyle(this.imageEl, width, height, startX, startY);
        const self = this;
        setTimeout(() => {
          ImagePreviewer.setImageAnimationParams(self.instanceNum, historyInfo);
        });
        document.documentElement.style.overflow = "hidden"; // Pour cacher la barre de défilemment vertical
        this.previewContainer.classList.add('show');
        !this.config.scrollbar && taggleScrollBar(false);
      }

    }, {
      key: "handleClose",
      value: function handleClose() {
        const opacity = this.config.opacity;
        const current = this.imageElements[this.index]; // console.log(isElementInViewport)
        const self = this;

        document.documentElement.style.overflow = "auto"; // Pour réafficher la barre de défilemment vertical
        if (isElementInViewport(current)) {
          runAnimation(this.previewContainer, {
            start: opacity,
            end: 0,
            step: -0.02,
            style: 'background',
            template: 'rgba(0, 0, 0, $)'
          }, () => {
            self.imageEl.style = '';
            self.imageEl.src = '';
            self.previewContainer.style.background = "";
            self.previewContainer.classList.remove('hiding');
            self.taggleModel(false);
          });

          const _getElementRect = getElementRect(current),
              top = _getElementRect.top,
              left = _getElementRect.left,
              width = _getElementRect.width,
              height = _getElementRect.height;

          this.imageEl.style.cssText = `width:${width}px;height:${height}px;position: fixed; top: ${top}px; left: ${left}px;`;
        } else {
          this.imageEl.style.display = 'none'; // image

          runAnimation(this.previewContainer, {
            start: opacity,
            end: 0,
            step: -0.05,
            style: 'background',
            template: 'rgba(0, 0, 0, $)'
          }, () => {
            self.imageEl.src = '';
            self.imageEl.style = '';
            self.previewContainer.style = "";
            self.previewContainer.classList.remove('hiding');
            self.taggleModel(false);
          });
        }

        this.previewContainer.classList.remove('show');
        this.previewContainer.classList.add('hiding');
        !this.config.scrollbar && taggleScrollBar(true);
      } // 向左旋转

    }, {
      key: "handelRotateLeft",
      value: function handelRotateLeft() {
        historyInfo.rotate -= 90;
        this.imageEl.style.setProperty('--rotate', `${historyInfo.rotate}deg`);
      } // 向右旋转

    }, {
      key: "handelRotateRight",
      value: function handelRotateRight() {
        historyInfo.rotate += 90;
        this.imageEl.style.setProperty('--rotate', `${historyInfo.rotate}deg`);
      } // 上一张

    }, {
      key: "prev",
      value: function prev() {
        if (this.index !== 0) {
          this.index -= 1; //TODO: 更新图片显示

          this.updateIndex(this.index);
          this.useIndexUpdateImage(this.index);
        }
      } // 下一张

    }, {
      key: "next",
      value: function next() {
        if (this.index < this.imageElements.length - 1) {
          this.index += 1;
          this.updateIndex(this.index);
          this.useIndexUpdateImage(this.index);
        }
      }

    }, {
      key: "useIndexUpdateImage",
      value: function useIndexUpdateImage(index) {
        const _this$imageElements$i = this.imageElements[index],
            height = _this$imageElements$i.height,
            width = _this$imageElements$i.width,
            src = _this$imageElements$i.src,
            ratio = this.config.ratio;

        this.imageEl.classList.add('moving');
        setImageBaseStyle(this.imageEl, width, height, windowWidth / 2 - width / 2, windowHeight / 2 - height / 2);
        historyInfo = {
          endX: 0,
          endY: 0,
          scale: calcScaleNums(width, height, ratio),
          rotate: 0
        };
        this.imageEl.src = src;
        ImagePreviewer.setImageAnimationParams(this.instanceNum, historyInfo);
        setTimeout(() => {
          this.imageEl.classList.remove('moving');
        });
      } // 更新视图上的索引

    }, {
      key: "updateIndex",
      value: function updateIndex(index) {
        this.index        = index;
        const description = this.imageElements[index].previousElementSibling;

        document.querySelectorAll(`#image-preview-container-${this.instanceNum} .image-description`).forEach(descriptionNode => {
          descriptionNode.innerHTML = (description && description.className === "commentaire" && description.innerHTML !== "") ? `<p>${description.innerHTML}</p>` : "";
        });
        document.querySelector(`#image-preview-container-${this.instanceNum} .total-nums`).innerText = this.imageElements.length;
        document.querySelector(`#image-preview-container-${this.instanceNum} .current-index`).innerText = index + 1;
      } // 渲染视图

    }, {
      key: "render",
      value: function render() {
        const template = "" +
          "<div class='preview-header'>" +
              "<div class='nums'>" +
                "<p><span class='current-index'></span> &nbsp;/&nbsp; <span class='total-nums'></span></p>" +
              "</div>" +
              "<div class='image-description'></div>" +
              "<div class='tool-btn'>" +
                `<button class='rotate-left' data-tooltip='${traductions.rotateRight}'><i class='iconfont icon-xuanzhuan'></i></button>` +
                `<button class='rotate-right' data-tooltip='${traductions.rotateLeft}'><i class='iconfont icon-xuanzhuan1'></i></button>` +
                `<button class='reset' data-tooltip='${traductions.reset}'><i class='iconfont icon-zhongzhi'></i></button>` +
                `<button class='close' data-tooltip='${traductions.close}'><i class='iconfont icon-account-practice-lesson-close'></i></button>` +
              "</div>" +
              "<div class='image-description'></div>" +
            "</div>" +
            "<div class='image-container'>" +
              `${this.imageElements.length > 1 ? `<button class='prev' data-tooltip='${traductions.previous}'><i class='iconfont icon-shangyige'></i></button>` : ""}` +
              "<div><img class='preview-image' src='' alt='' /></div>" +
              `${this.imageElements.length > 1 ? `<button class='next' data-tooltip='${traductions.next}'><i class='iconfont icon-xiayige'></i></button>` : ""}` +
            "</div>";

        const el = document.getElementById(`image-preview-container-${this.instanceNum}`);

        if (!el) {
          this.previewContainer = document.createElement('div');
          this.previewContainer.classList.add('image-preview-container');
          this.previewContainer.id = `image-preview-container-${this.instanceNum}`;
          this.previewContainer.innerHTML = template;

          _BODY.appendChild(this.previewContainer);
        } else {
          console.log("Le 'image-preview-container' existe déjà, les medias seront affichées dedans.");
          this.previewContainer = el;
        }

        this.imageEl = this.previewContainer.querySelector('.preview-image');
      } // 初始化

    }, {
      key: "init",
      value: function init() {
        this.imageElements = this.selector.querySelectorAll("img");
        windowHeight       = window.innerHeight;
        windowWidth        = window.innerWidth;
        _BODY              = document && document.body || document.getElementsByTagName('body')[0];

        for (let i = 0; i < this.imageElements.length; i++)
          this.imageElements[i].classList.add('zoom-in');

        this.instanceNum = numberOfInstances;
        this.render();
        this.updateIndex(this.index);
        this.bindEvent(this.imageElements);
        this.options.onInited && this.options.onInited();
        numberOfInstances++;
      }

    }], [{
      key: "setImageAnimationParams",
      value: function setImageAnimationParams(previewerIndex, historyInfo) {
        const imageEl = document.querySelector(`#image-preview-container-${previewerIndex} .preview-image`);
        imageEl.style.setProperty('--offsetX', `${historyInfo.endX}px`);
        imageEl.style.setProperty('--offsetY', `${historyInfo.endY + 30}px`);
        imageEl.style.setProperty('--scale', historyInfo.scale.toFixed(2));
        imageEl.style.setProperty('--rotate', `${historyInfo.rotate}deg`);
      }
    }]);

    return ImagePreviewer;
  }();
})));
