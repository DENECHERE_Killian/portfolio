const FORWARD = 0;
const BACKWARD = 1;

class Carousel {
	constructor(carouselEl) {
		const mediaElList = carouselEl.querySelectorAll('img, video');

		// Buttons to change displayed picture
		mediaElList[0].onclick = () => this.#slide(BACKWARD);
		mediaElList[mediaElList.length - 1].onclick = () => this.#slide(FORWARD);

		this.shownMediaIndex = 0;
		this.bubblesList = carouselEl.querySelector('ol');
		this.mediaList = Array.prototype.slice.call(mediaElList).splice(1, mediaElList.length - 2);

		if (this.mediaList.length === 0)
			return;

		// Zoom on picture when click
		const imgPreviewerId = new ImgPreviewer(carouselEl.querySelector('ul'), {scrollbar: false}).instanceNum;
		const previewButtons = document.querySelectorAll(`#image-preview-container-${imgPreviewerId} > div.image-container > button`);
		previewButtons.forEach(button => button.onclick = () => this.#slide(button.classList.value === 'next' ? FORWARD : BACKWARD));

		this.mediaList.forEach((media, index) => {
			// Slide events
			media.addEventListener('swiped', (e) => {
				if (e.detail.dir === 'right')
					this.#slide(BACKWARD);

				if (e.detail.dir === 'left')
					this.#slide(FORWARD);
			});

			// Navigation bubbles
			const buble = document.createElement('div');
			if (index === 0) // The first is selected and shown by default
				buble.setAttribute('class', 'actif'); // Color the bubble
			else
				media.style.transform = 'translateX(100vw)'; // Hide other images than the first

			buble.onclick = () => this.#goToMedia(index);
			this.bubblesList.appendChild(buble);
		});
	}

	#goToMedia(index) {
		if (this.mediaList.length === 0)
			return;

		const diff = Math.abs(index - this.shownMediaIndex);

		if (diff < 0)
			return;

		for (let i = 0; i < diff; i++)
			this.#slide(this.shownMediaIndex > index ? BACKWARD : FORWARD);
	}

	#slide(direction) {
		if (this.mediaList.length === 0)
			return;

		this.bubblesList.children[this.shownMediaIndex]?.removeAttribute('class');

		if (direction === BACKWARD && this.shownMediaIndex > 0) {
			this.#translateCurrentMedia('100vw'); // Move current picture
			this.shownMediaIndex -= 1; // Set new picture
		}

		if (direction === FORWARD && this.shownMediaIndex < this.mediaList.length - 1) {
			this.#translateCurrentMedia('-100vw'); // Move current picture
			this.shownMediaIndex += 1; // Set new picture
		}


		// Show new picture
		this.#translateCurrentMedia('0');
		this.bubblesList.children[this.shownMediaIndex]?.setAttribute('class', 'actif');
	}

	#translateCurrentMedia(position) {
		const media = this.mediaList[this.shownMediaIndex];

		if (!media)
			return;

		media.style.transform = `translateX(${position})`;

		if (media.tagName === 'VIDEO') {
			if (position === '0') {
				// Make video restart when shown
				// media.pause();
				media.currentTime = 0;
				media.play();
			} else {
				media.pause();
			}
		}
	}
}

// Affect a Carousel to each html carousel class element
Array.prototype.slice.call(document.querySelectorAll('.carousel')).forEach(carousel => new Carousel(carousel));
