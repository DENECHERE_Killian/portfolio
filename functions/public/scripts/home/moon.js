/* Animation des cercles du header */
const pathsDOM = document.querySelectorAll('header > div > svg path');
const paths    = [pathsDOM[0], pathsDOM[1], pathsDOM[4]];

for (let i = 0; i < paths.length; i++) {
  const path = paths[i];
  const offset = anime.setDashoffset(path);
  path.setAttribute('stroke-dashoffset', offset);
  anime({
    targets: paths[i],
    strokeDashoffset: [offset, 0],
    duration: anime.random(2500, 5000),
    delay: anime.random(0, 2000),
    loop: true,
    direction: 'alternate',
    easing: 'easeInOutSine'
  });
}
