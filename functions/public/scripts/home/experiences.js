sectionExperiences.querySelectorAll('menu > ol > li').forEach(button => button.onclick = () => sortExperiences(button)); // Événements des boutons de tri

checkForMobileView();
window.addEventListener('resize', checkForMobileView);

// Set the click event of hide buttons
sectionExperiences.querySelectorAll('ul > li > hidebutton').forEach(button => {
  button.onclick = () => {
    if (!isOnMobile())
      return;

    const experienceDescription = button.previousElementSibling;
    const partsList             = document.querySelector('#projects > ul');
    const descHidden            = experienceDescription.style.height === '0px';
    button.querySelectorAll('img').forEach(img => img.style.transform = descHidden ? 'rotate(180deg)' : 'rotate(0deg)');

    anime({
      targets: experienceDescription,
      height: descHidden ? experienceDescription.scrollHeight : '0',
      duration: 1000,
      update: () => {
        if (descHidden)
          partsList.style.height = partsList.scrollHeight + (experienceDescription.scrollHeight/60) + 'px';
        else
          partsList.style.height = partsList.scrollHeight - (experienceDescription.scrollHeight/60) + 'px';
      },
      easing: 'easeInOutQuint',
      after: () => {
        const selectedTabIndex = Array.from(menuButtons).indexOf(Array.from(menuButtons).find((part) => part.attributes.class));
        partsList.style.height = `${partsList.children[selectedTabIndex].clientHeight}px`;
      }
    });
  };
});


// Fonction de tri
function sortExperiences(li) {
  const nomExperience = li.textContent;
  const isFirst       = li.parentNode.firstElementChild === li;

  // Change la couleur de l'option sélectionnée
  if (li.hasAttribute('selected')) {
    li.removeAttribute('selected'); // Désélectionner

    if (isFirst)
      listeTypeEvenements.forEach(nomEvenement => changeVisibilityExperience(nomEvenement, true));

    changeVisibilityExperience(nomExperience, true);

    // Si on n'avait plus rien d'affiché, on affiche tout
    if (Array.from(sectionExperiences.querySelector('ul').children).every(child => child.style.display === 'none')) {
      listeTypeEvenements.forEach(nomEvenement => changeVisibilityExperience(nomEvenement, false))
      sectionExperiences.querySelector('menu > ol > li:first-child').setAttribute('selected', '');
    }
    return ;
  }

  // Le bouton n'est pas sélectionné
  li.setAttribute('selected', '');

  // En fonction de la nouvelle sélection, on va sélectionner ou désélectionner d'autres options
  if (isFirst) {
    listeTypeEvenements.forEach(nomEvenement => changeVisibilityExperience(nomEvenement, false));

    // Désélectionne les autres options
    for (let index = 0; index < li.parentNode.children.length; index++) {
      if (index > 0)
        li.parentNode.children[index].removeAttribute('selected');
    }

  } else {

    // Désélectionne l'option Tous
    const child = li.parentNode.children[0];
    if (child.hasAttribute('selected')) {
      listeTypeEvenements.forEach(nomEvenement => changeVisibilityExperience(nomEvenement, true))
      child.removeAttribute('selected');
    }

    changeVisibilityExperience(nomExperience, false);
  }
}

// Afficher ou chacher certaines expériences en fonction d'une partie
function changeVisibilityExperience(title, hide) {
  for (experience of sectionExperiences.querySelectorAll('ul > li')) {
    if (experience.querySelector('div > p').innerText.toUpperCase() === title.toUpperCase())
      experience.style.display = hide ? 'None': 'flex';
  }
}

// Afficher/cacher la description des expériences sur mobile
function checkForMobileView() {
  sectionExperiences.querySelectorAll('ul > li').forEach(experience => {
    experience.querySelector('hidebutton').style.display = isOnMobile() ? 'flex' : 'none';
    experience.querySelector('li > p').style.height = isOnMobile() ? '0' : '';
  });

  updateCurrentTabHeightOnMobile();
}
