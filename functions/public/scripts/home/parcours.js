const sectionExperiences = document.querySelector("#projects > ul > li:first-child");
const sectionProjets = document.querySelector("#projects > ul > li:nth-child(2)");
const sectionParcours = document.querySelector("#projects > ul > li:last-child");
const menuButtons = document.querySelectorAll("#projects > menu > li");
let enableCareerAnimation = true;


// === Parcours ===

// Animations des étapes du parcours
function animateCareerElement(li) {
	if (!li.attributes.origine)
		return console.log("Pas d'attribu origine sur le parcours");

	setTimeout(() => {
		anime({
			targets: li,
			translateX: () => {
				if (li.attributes.origine.value === "droite")
					return ["200%", "115%"];

				return ["-50vw", "3vw"];
			},
			easing: "easeOutQuart",
			duration: 500,
			complete: () => {
				if (li.nextElementSibling)
					startCareerAnimation(li.nextElementSibling);
			}
		});
	}, 1000);
}

// Lance l'animation des éléments du parcours
function startCareerAnimation(li) {
	const verticalTimeLine = sectionParcours.querySelector("article > div");
	const verticalTimeLineInfos = verticalTimeLine.getBoundingClientRect();
	const carrerPathEventInfos = li.getBoundingClientRect();

	anime({
		targets: verticalTimeLine,
		height: carrerPathEventInfos.y - verticalTimeLineInfos.y + (carrerPathEventInfos.height / 2) + ((window.innerWidth / 100) * 0.5) - 1,
		duration: 1000
	});

	animateCareerElement(li);
}


// === Commun entre les 3 parties ===

// Transition entre les parties project et parcours
function displayProjectsSectionParts(menuButton, buttonOrderIndex) {
	anime({
		targets: "#projects > ul > li",
		translateX: (el, i) => {
			switch (buttonOrderIndex) {
				case -1: // Expériences pros
					if (i === 0) return 0;
					if (i === 1 || i === 2) return "100vw";
					break;
				case 0: // Projets
					if (i === 0) return "-100vw";
					if (i === 1) return 0;
					if (i === 2) return "100vw";
					break;
				case 1: // Parcours
					if (i === 0 || i === 1) return "-100vw";
					if (i === 2) return 0;
					break;
				default:
					console.log("Problème décalage parcours et projects");
			}
		},
		easing: "easeOutExpo",
		duration: 2000,
		delay: anime.stagger(100),
		begin: () => {
			menuButtons.forEach(button => {
				button.removeAttribute("class");
			});

			menuButton.setAttribute("class", "underline");
			sectionParcours.querySelector("article").scrollTo(0, 0);
			sectionExperiences.querySelector("ul").scrollTo(0, 0);
			updateCurrentTabHeightOnMobile();
		},
		complete: () => {
			// The button that was pressed was the career one.
			if (buttonOrderIndex === 1 && enableCareerAnimation) {
				startCareerAnimation(sectionParcours.querySelector("article > ul > li"));
				enableCareerAnimation = false;
			}
		}
	});
}

function updateCurrentTabHeightOnMobile() {
	// Set the size of the <ul> on mobile
	const partsList = document.querySelector("#projects > ul");
	partsList.style.height = ``; // Reset to default

	if (!isOnMobile())
		return null;

	const selectedTabIndex = Array.from(menuButtons).indexOf(Array.from(menuButtons).find((part) => part.attributes.class));
	partsList.style.height = `${partsList.children[selectedTabIndex].clientHeight}px`;
}

updateCurrentTabHeightOnMobile();
window.addEventListener("resize", updateCurrentTabHeightOnMobile);

// Transition entre les parties avec un swipe pour les mobiles
function handleSwipe(id, e) {
	if (Math.abs(e.detail.xEnd - e.detail.xStart) < (window.innerWidth / 3))
		return; // Il faut swipe sur le tier de la largeur de l'écran pour activer la fonction

	if (e.detail.dir === "left" && (id === -1 || id === 0))
		displayProjectsSectionParts(menuButtons[id + 2], id + 1);
	else if (e.detail.dir === "right" && (id === 0 || id === 1))
		displayProjectsSectionParts(menuButtons[id], id - 1);
}

// Événements des boutons du menu
menuButtons.forEach((button, index) => {
	button.onclick = () => displayProjectsSectionParts(button, index - 1);
});

// Événements actifs sur mobile
sectionExperiences.addEventListener("swiped", (e) => handleSwipe(-1, e));
sectionParcours.addEventListener("swiped", (e) => handleSwipe(1, e));
sectionProjets.addEventListener("swiped", (e) => handleSwipe(0, e));
