let starsScreenZone  = document.querySelector("#backgroundAnimation > ul:nth-child(2)"); // Pour les étoiles
let starsZoneInfos   = starsScreenZone.getBoundingClientRect();
let waitBeforeResize = false;
let interval;
fillBackgroundWithStars();

function createStar() {
  const posX = anime.random(0, starsZoneInfos.height);
  const posY = anime.random(0, starsZoneInfos.width); // La zone touchant le haut, on démarre à 0 et on ajoute la hauteur

  const etoile = document.createElement("li");
  etoile.setAttribute("class", "etoiles");
  etoile.style.height          = `0.${anime.random(2, 5)}em`;
  etoile.style.width           = etoile.style.height;
  etoile.style.top             = `${posX}px`;
  etoile.style.left            = `${posY}px`;
  etoile.style.backgroundColor = ["var(--blanc)", "var(--flash-color)", "var(--second-bg-color)"][anime.random(0, 2)];

  starsScreenZone.appendChild(etoile);
}

// Resize background when window size change
window.addEventListener("resize", function() {
  if (waitBeforeResize)
    return ;

  waitBeforeResize = true;
  setTimeout(function() {
    while (starsScreenZone.firstChild)
     starsScreenZone.removeChild(starsScreenZone.firstChild);

    starsZoneInfos = starsScreenZone.getBoundingClientRect();
    fillBackgroundWithStars();
    waitBeforeResize = false;
  }, 1000);
});

function fillBackgroundWithStars() {
  for (let i=0; i<anime.random(200, 250); i++)
    createStar();

  if (interval)
    clearInterval(interval);

  interval = setInterval(() =>{
    // Stars blink animation
    anime({
      targets: starsScreenZone.children[anime.random(0, starsScreenZone.children.length-1)],
      opacity: [1, 0],
      direction: "alternate",
      duration: 1000
    });
  }, 100);
}
