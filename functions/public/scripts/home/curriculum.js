let displayedPartIndex  = 0;
let choosedOptions      = [];
const curriculumSection = document.querySelector('#curriculum > div');
const partsNumber       = curriculumSection.querySelector('ul').childElementCount;
const optionsList       = curriculumSection.querySelectorAll('ul > li');
const closeButton       = curriculumSection.querySelector('nav > button:first-child');
const backButton        = curriculumSection.querySelector('nav > button:nth-child(2)');
backButton.onclick      = showPreviousPart; // Set back button function
closeButton.onclick     = closeOverlay; // Set close button function

curriculumSection.querySelectorAll('ul > li:not(li:last-child) > ol > li').forEach(part => part.onclick = showNextPart);// Set the onclick personnalisation options
curriculumSection.querySelector('div > step > total').innerHTML = partsNumber; // Set parts number
document.querySelector('footer > ul > li:nth-child(2)').onclick = showOverlay; // Set footer button

function showNextPart() {
    optionsList[displayedPartIndex].style.display = 'none'; // Hide the actual part
    displayedPartIndex ++;
    optionsList[displayedPartIndex].style.display = 'flex'; // Show the new part

    // Back button
    if (backButton.style.visibility !== 'initial') {
        backButton.style.display = 'flex';
        closeButton.style.display = 'none';
    }

    // Last part
    if (displayedPartIndex+1 === partsNumber) {
        anime({
            targets: [optionsList[displayedPartIndex].querySelector('description'), optionsList[displayedPartIndex].querySelector('ol')],
            easing: 'easeOutCubic',
            duration: 700,
            flex: '1'
        });
    }

    // Save option choosing
    choosedOptions.push(this.attributes.value.value);

    actualiseProgress();
}

function showPreviousPart() {
    optionsList[displayedPartIndex].style.display = 'none'; // Hide the actual part
    displayedPartIndex --;
    optionsList[displayedPartIndex].style.display = 'flex'; // Show the new part

    // First option
    if (displayedPartIndex === 0) {
        backButton.style.display  = 'none';
        closeButton.style.display = 'flex';
    }

    // Save choosen option
    choosedOptions.pop();

    actualiseProgress();
}

function showOverlay() {
    actualiseProgress();
    document.querySelector('html').style.overflow            = 'hidden';
    document.querySelector('#curriculum').style.display      = 'flex'; // Show Overlay
    curriculumSection.querySelector('ul > li').style.display = 'flex'; // Show first option
}

function closeOverlay() {
    // Reset parameters & buttons
    displayedPartIndex = 0;
    choosedOptions     = [];
    backButton.style.display  = 'none';
    closeButton.style.display = 'flex';

    document.querySelector('html').style.overflow       = 'auto';
    document.querySelector('#curriculum').style.display = 'none'; // Hide Overlay
    curriculumSection.querySelectorAll('ul > li').forEach((part, index) => { // Hide all options after the first one
        if (index > 0)
            part.style.display = 'none';
    });
}

function actualiseProgress() {
    const progressBar = curriculumSection.querySelector('progress');

    // Progress animation
    anime({
        targets: progressBar,
        easing: 'easeOutExpo',
        duration: 2000,
        value: ((displayedPartIndex+1)/partsNumber)*100
    });

    // Update parts Number
    curriculumSection.querySelector('div > step > current').innerHTML = displayedPartIndex+1;

    refreshPDFLinks()
}

function refreshPDFLinks() {
    const buttonsList = curriculumSection.querySelectorAll('ul > li:last-child > ol > li > button');
    const pdfFile = `/documents/CV_denechere_killian_${choosedOptions.toString().replace(' ,', '_')}.pdf`;

    // View file if possible
    if (isOnMobile())
        buttonsList[0].style.display = 'none';
    else
        buttonsList[0].onclick = () => {
            window.open(pdfFile, '_blank');
            window.focus();
            closeOverlay();
        }

    // Download file
    buttonsList[1].onclick = () => {
        let anchor = document.createElement('a');
        anchor.href = pdfFile;
        anchor.target = '_blank'; // Un the cas the browser doesn't support it, the pdf will be oppenend in another tab.
        anchor.download = 'Curriculum_Killian_Denechere';
        anchor.click();
        closeOverlay();
    }
}

// This event is required for firefox on mobile that change the size of the window whitout updating the css each time its navigation bar apear of dissapear
window.addEventListener('resize', () => {
    document.querySelector('#curriculum').style.height = `${window.innerHeight}px`;
})