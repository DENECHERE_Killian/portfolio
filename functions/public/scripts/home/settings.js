const boxOptions = document.querySelector("#options > section");
let affiche      = false;

document.querySelector("#options > img").onclick = function() {
  boxOptions.style.left = affiche ? "-50em" : "5em";
  affiche = !affiche;
}

// Change background color when displaying the pojects list
window.addEventListener("scroll", () => {
  if (boxOptions.getBoundingClientRect().height + window.scrollY >= document.querySelector("#projects > ul").offsetTop)
    boxOptions.style.boxShadow = "var(--box-shadow), -0.5em 0.5em var(--main-bg-color)";
  else
    boxOptions.style.boxShadow = "";
});
