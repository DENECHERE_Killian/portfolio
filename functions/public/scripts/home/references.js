const carousel = document.querySelector("#references > ul");
const speed    = 2;
let mouseover  = false;

carousel.onmouseover  = () => mouseover = true;
carousel.onmouseout   = () => mouseover = false;
carousel.ontouchstart = () => mouseover = true;
carousel.ontouchend   = () => mouseover = false;

window.addEventListener('load', () => {
  let previousScroll;
  let goingLeft = true;

  setInterval(() => {
    if (mouseover)
      return ;

    if (carousel.scrollLeft !== carousel.scrollWidth)
      carousel.scrollTo({ left: goingLeft ? carousel.scrollLeft += speed : carousel.scrollLeft -= speed, behavior: 'smooth'});

    if (previousScroll === carousel.scrollLeft) // Change direction when on end of the sharedIcons list
      goingLeft = !goingLeft;

    previousScroll = carousel.scrollLeft;
  }, 15);

  // Attach the handler
  carousel.addEventListener('mousedown', mouseDownHandler);
  carousel.addEventListener('touchstart', mouseDownHandler, { passive: true });
});


carousel.style.cursor = 'grab';
let pos = { top: 0, left: 0, x: 0, y: 0 };

const mouseDownHandler = function (e) {
  carousel.style.cursor = 'grabbing';
  carousel.style.userScarouselct = 'none';

  pos = {
    left: carousel.scrollLeft,
    top: carousel.scrollTop,
    // In case of touch event
    x: e.clientX ?? e.touches[0].clientX,
    y: e.clientY ?? e.touches[0].clientY,
  };

  document.addEventListener('mousemove', mouseMoveHandler);
  document.addEventListener('touchmove', mouseMoveHandler);
  document.addEventListener('mouseup', mouseUpHandler);
  document.addEventListener('touchend', mouseUpHandler);
};

const mouseMoveHandler = function (e) {
  // How far the mouse has been moved
  const dx = (e.clientX ?? e.touches[0].clientX) - pos.x;
  const dy = (e.clientY ?? e.touches[0].clientY) - pos.y;

  // Scroll the element
  carousel.scrollTop  = pos.top - dy;
  carousel.scrollLeft = pos.left - dx;
};

const mouseUpHandler = function () {
  carousel.style.cursor = 'grab';
  carousel.style.removeProperty('user-select');

  document.removeEventListener('mousemove', mouseMoveHandler);
  document.removeEventListener('touchmove', mouseMoveHandler);
  document.removeEventListener('mouseup', mouseUpHandler);
  document.removeEventListener('touchend', mouseUpHandler);
};
