// ======= COMMUN =======

function isOnMobile() {
  return /Android|webOS|iPhone|iPad|iPod|pocket|psp|kindle|avantgo|blazer|midori|Tablet|Palm|maemo|plucker|phone|BlackBerry|symbian|IEMobile|mobile|ZuneWP7|Windows Phone|Opera Mini/i.test(navigator.userAgent);
}


// ======== HEAD ========

// Animation du titre
const titres = document.querySelectorAll('header > hgroup > h1 > span > span');
titres.forEach(titre => titre.innerHTML = titre.textContent.replace(/\S/g, "<span class='letter'>$&</span>"))

anime({
  targets: '.letter',
  translateY: ["2.5em", 0],
  translateZ: 0,
  duration: 750,
  delay: (el, i) => 50 * i,
  complete: function() {
    // Appartition des différentes parties de la page après avoir affiché le titre
    ScrollReveal().reveal("header > div, #backgroundAnimation > ul:nth-child(2), header > span", { delay: 200, interval: 700, easing: 'cubic-bezier(0.645, 0.045, 0.355, 1)' });
  }
});


// Animation des titres des cartes de projects
const titresCartes = document.querySelectorAll('.carte > section > h4');
titresCartes.forEach(titre => titre.innerHTML = titre.textContent.replace(/\S/g, "<span class='h4-letter'>$&</span>"))

anime({
  targets: ".h4-letter",
  keyframes: [{ translateY: "-0.5em" }, { translateY: "0em" }],
  loop: true,
  easing: 'cubicBezier(0.645, 0.045, 0.355, 1)',
  delay: anime.stagger(100),
  endDelay: 3000
});


// ======== PROFIL ========

const terminal     = new Termynal('terminal', { progressLength: 15, noInit: true, typeDelay: 10 });
const profilNode   = document.querySelector("profil");
const imageProfil  = profilNode.querySelector('img');
const terminalNode = document.querySelector("terminal");

// Choix random de l'image de profil
function loadProfilPicture() {
  let random;
  do {
    random = anime.random(1, 4);
  } while(random === Number(imageProfil.attributes.src.value?.split('man_')[1]?.split('.svg')[0]) ?? -1)

  imageProfil.setAttribute('src', `medias/icons/home/man_${random}.svg`);
}
imageProfil.onclick = loadProfilPicture;
loadProfilPicture();

ScrollReveal().reveal("terminal", { delay: 200, interval: 700, easing: 'cubic-bezier(0.645, 0.045, 0.355, 1)', afterReveal: () => {
  terminalNode.querySelectorAll("p").forEach((p) => p.style.display = "block");
  profilNode.style.display = "flex";
  terminal.init(!isOnMobile());
  terminalNode.prepend(profilNode);
  setInterval(changeProfilSentence, 5000);
}});

let lastSentenceIndex = null;
function changeProfilSentence() {
  // Animation du changement de 'je suis ...'
  const jeSuis        = profilNode.querySelectorAll('h2')[1];
  const listeJeSuisFR = ["Passionné par le dévelopemment",
                      "Moi (enfin, je crois)",
                      "Curieux et impliqué",
                      "Attentif",
                      "Organisé",
                      "Un 'Défenseur' d'après le test de <a title='En savoir plus' href='https://www.16personalities.com/fr/la-personnalite-isfj' target='_blank'>16personalities.com</a>"];
  const listeJeSuisEN = ["Passionate about development",
                      "Me (well, I believe)",
                      "Curious and involved",
                      "Mindfull",
                      "Organized",
                      "A 'Defender' according to the test of <a title='Read more' href='https://www.16personalities.com/fr/la-personnalite-isfj' target='_blank'>16personalities.com</a>"];

  jeSuis.style.transform = "translateX(100vw)";
  setTimeout(() => {
    let randomIndex = lastSentenceIndex;
    while(randomIndex === lastSentenceIndex)
      randomIndex = anime.random(0, listeJeSuisFR.length-1);

    lastSentenceIndex = randomIndex;
    jeSuis.innerHTML = lang === 'fr' ? listeJeSuisFR[randomIndex] : jeSuis.innerHTML = listeJeSuisEN[randomIndex];
    jeSuis.style.transform = "";
  }, 1000);
}

function scrollToParcours() {
  window.scrollTo({
    top: document.querySelector("#projects").offsetTop
  });
}


// ======== FOOTER ========
function copy(elem, valeur) {
  let tempInput   = document.createElement("input");
  tempInput.style = "position: absolute; left: -1000px; top: -1000px";
  tempInput.value = valeur;
  document.body.appendChild(tempInput);
  tempInput.select();
  document.execCommand("copy");
  document.body.removeChild(tempInput);

  validationFooter(elem, lang === 'fr' ? "Copié" : "Copied");
}

function validationFooter(elem, message) {
  elem.parentNode.style.backgroundColor   = "var(--vert)";
  elem.previousElementSibling.style.color = "var(--blanc)";
  const ancienTexte = elem.previousElementSibling.innerHTML;
  elem.previousElementSibling.innerHTML       = message;
  elem.previousElementSibling.style.width     = "100%";
  elem.previousElementSibling.style.height    = "100%";
  elem.previousElementSibling.style.display   = "flex";
  elem.previousElementSibling.style.transform = "translatey(0)";
  setTimeout(function() {
    elem.previousElementSibling.style.transform = "";
    elem.previousElementSibling.style.color     = "";
    elem.previousElementSibling.style.display   = "";
    elem.previousElementSibling.style.height    = "";
    elem.previousElementSibling.style.width     = "";
    elem.previousElementSibling.innerHTML       = ancienTexte;
    elem.parentNode.style.backgroundColor       = "transparent";
  }, 2000);
}
