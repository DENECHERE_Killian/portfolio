require('dotenv').config();
const {onRequest} = require("firebase-functions/v2/https"); // The Cloud Functions for Firebase SDK to create Cloud Functions and set up triggers.
const compression = require('compression'); // Compresse tous les fichiers lors du chargement d'une page
const i18n = require('i18n-express'); // Pour gérer les traductions de l'interface
const express = require('express'); // Gère les différentes routes du serveur
const exec = require('child_process').exec; // Exécute des commandes (pour obtenir la dernière date de commit)
const fs = require('fs'); // Lit le contenu des fichiers project
const path = require('path');
const staticFolder = 'public';
const urls = {
    sharedScripts: `/scripts/`,
    sharedStyles: `/styles/`,
    sharedIcons: '/medias/icons/',

    homeImages: `/medias/icons/home/`,
    homeScripts: `/scripts/home/`,
    homeStyles: `/styles/home/`,
    homeIcons: '/medias/icons/home/',

    projectStyles: `/styles/project/`,
    projectScripts: `/scripts/project/`,
    projectIcons: `/medias/icons/project/`,

    notFoundPageStyles: `/styles/404/`,
    notFoundPageIcons: `/medias/icons/404/`,

    activities: `/medias/activities/`,
    projects: `/medias/projects/`,
    references: `/medias/references/`,
    favicons: '/medias/favicons/',
};

const app = express(); // Le serveur gérant les routes et les vues
// app.use(`/${staticFolder}/`, express.static(staticFolder));
app.set('views', path.join(__dirname, `views`));
app.set('view engine', 'html');


app.use(i18n({
    translationsPath: path.join(__dirname, `${staticFolder}/contents/languages`),
    siteLangs: ['fr', 'en'],
    defaultLang: 'fr',
    paramLangName: 'lang',
    textsVarName: 'translation'
}));

app.use(express.urlencoded({extended: true}));
app.use(compression()); // Compress files
app.engine('html', require('ejs').renderFile);

const isMinified = (process.env.MINIFY === "true") ? ".min" : "";
let conceptions = JSON.parse(fs.readFileSync(`./${staticFolder}/contents/conceptions.json`).toString());
let portfolio = JSON.parse(fs.readFileSync(`./${staticFolder}/contents/portfolio.json`).toString());
let erreurs = JSON.parse(fs.readFileSync(`./${staticFolder}/contents/errors.json`).toString());


// -------------------
//      LISTENERS
// -------------------

// Page d'accueil
app.get('/', displayHomePage);

// Page d'un projet
app.get('/project/:proj/', displayProjectPage);
app.get('/project/:proj/:idMain', displayProjectPage);

// Page 404
app.use(display404Page);


// -------------------
//      FONCTIONS
// -------------------

function getLang(req) {
    return req.i18n_lang;
}

// -------------------
// FONCTIONS AFFICHAGE
// -------------------

function displayHomePage(req, res) {
    const projectFilesOrder = JSON.parse(fs.readFileSync(`${staticFolder}/contents/projects/order.json`).toString());
    portfolio = JSON.parse(fs.readFileSync(`${staticFolder}/contents/portfolio.json`).toString());
    let projects = [];

    for (const filename of projectFilesOrder) {
        // if (filename.startsWith('working'))
        // 	continue;
        try {
            projects.push(JSON.parse(fs.readFileSync(`${staticFolder}/contents/projects/${filename}.json`).toString()));
        } catch (err) {
            console.warn(`Une erreur est survenu lors de la lecture du fichier ${staticFolder}/contents/projects/${filename}.json\n "${err}"`);
        }
    }

    // Obtention de la date de dernière modification du project
    exec("curl https://gitlab.com/api/v4/projects/19761628/repository/commits", async (error, date) => {
        date = new Date(JSON.parse(date)[0].committed_date);
        // Retrieve experiences
        let listeTousTypeEvenements = [];
        portfolio.experiences.forEach((experience) => {
            listeTousTypeEvenements.push(experience.type[getLang(req)]);
        });
        const listeTypeUniqueEvenements = [...new Set(listeTousTypeEvenements)];

        res.render("home.ejs", {
            userComefromProjectPage: (req.headers.referer !== undefined && req.headers.referer.includes('project')),
            listeTypeEvenements: listeTypeUniqueEvenements,
            projectsList: projects,
            dateModif: date.toLocaleDateString("en-UK"),
            portfolio: portfolio,
            isMinified: isMinified,
            lang: getLang(req),
            urls: urls,
            styleFiles: [
                `${urls.homeStyles}header${isMinified}.css`,
                `${urls.homeStyles}main${isMinified}.css`,
                `${urls.homeStyles}projects${isMinified}.css`,
                `${urls.homeStyles}parcours${isMinified}.css`,
                `${urls.homeStyles}experiences${isMinified}.css`,
                `${urls.homeStyles}activites${isMinified}.css`,
                `${urls.homeStyles}references${isMinified}.css`,
                `${urls.homeStyles}cards${isMinified}.css`,
                `${urls.homeStyles}curriculum${isMinified}.css`,
                `${urls.homeStyles}footer${isMinified}.css`,
                `${urls.sharedStyles}termynal/termynal${isMinified}.css`
            ],
            scriptFiles: [
                `${urls.sharedScripts}termynal/termynal${isMinified}.js`,
                `${urls.sharedScripts}swipedEvent/swipedEvent.min.js`,
                `${urls.homeScripts}main${isMinified}.js`,
                `${urls.homeScripts}curriculum${isMinified}.js`,
                `${urls.homeScripts}stars${isMinified}.js`,
                `${urls.homeScripts}settings${isMinified}.js`,
                `${urls.homeScripts}parcours${isMinified}.js`,
                `${urls.homeScripts}experiences${isMinified}.js`,
                `${urls.homeScripts}references${isMinified}.js`
            ]
        });
    });
}

function displayProjectPage(req, res) {
    if (!req.params.idMain)
        req.params.idMain = 0;

    try {
        let projectDatas = JSON.parse(fs.readFileSync(`${staticFolder}/contents/projects/${req.params.proj}.json`).toString())

        if (projectDatas.carte.redirection !== "") {
            console.warn(`Redirection effectée`);
            return res.redirect(projectDatas.carte.redirection);
        }

        res.render("project.ejs", {
            project: projectDatas,
            idMainUrl: req.params.idMain,
            lang: getLang(req),
            isMinified: isMinified,
            urls: urls,
            portfolio: portfolio,
            conceptions: conceptions,
            erreurs: erreurs
        });
    } catch (err) {
        console.error(`Une erreur est survenu lors de la lecture des informations du project "${err}"`);
        res.redirect("/");
    }
}

function display404Page(req, res) {
    res.render('404.ejs', {
        portfolio: portfolio,
        urls: urls,
        isMinified: isMinified,
        lang: getLang(req)
    });
}


// -------------------
//      SERVEUR
// -------------------

// // Le port est aussi défini sur le serveur heroku
// app.listen(3000, () => {
// 	console.log(`Serveur lancé -> http://localhost:3000`);
// 	console.log(`Les fichiers utilisés sont minifyed : ${process.env.MINIFY === "true"}`);
// });

exports.app = onRequest(app);