const UglifyJs           = require("uglify-js");
const UglifyCss          = require('uglifycss');
const fs                 = require("fs");
const publicFolder       = "functions/public";
const cacheFileName      = "compress_cache.json";
const optionsCompressCSS = { debug: true };
const optionsCompressJS  = { mangle: { toplevel: true, reserved: ["Termynal", "updateCurrentTabHeightOnMobile", "afficherPartie", "scrollToParcours", "isOnMobile", "sectionExperiences", "copy", "afficherRessource", "anime" ] }, compress: true };
// Certains noms de méthodes js sont dans la liste "reserved" ce qui signifie que ces noms resterons les mêmes lors de la compression du fichier.
// Il faut faire ça, car ces mêmes noms sont appelés depuis les pages html lors de différents évenements : il faut les attribuer via js pour palier à cela.

function compresserFichierJS(fileName) {
  if (fileName.endsWith('.js'))
    return console.log("Le fichier à compressser ne doit pas avoir d'extension");

  const code   = fs.readFileSync(`${fileName}.js`, "utf8");
  const result = UglifyJs.minify(code, optionsCompressJS);
  if (result.error)
    return console.log(`Une erreur est survenue lors de la compression d'un fichier js`, result);

  // console.log(result.code);
  fs.writeFileSync(`${fileName}.min.js`, result.code, "utf8");
}

function compresserFichierCSS(filename) {
  fs.writeFile(`${publicFolder}/styles/${filename}.min.css`,
                UglifyCss.processFiles([`${publicFolder}/styles/${filename}.css`],
                optionsCompressCSS), (err) => {
    if(err)
      return console.log(`Une erreur est survenue lors de l'écriture du fichier css compressé "${err}"`);
  });
}

function compresserTousFichiersJS() {
  fs.writeFile(cacheFileName, "{}", (err) => {
    if(err)
      return console.log(`Une erreur est survenue lors de la création du fichier de cache${err}`);

    try {
      optionsCompressJS.nameCache = JSON.parse(fs.readFileSync(cacheFileName, "utf8"));
    } catch (err) {
      console.error(`Une erreur est survenu lors de la création du cache "${err}"`);
    }

    // Lecture de chaque fichier js du project
    try {
      fs.readdirSync(`${publicFolder}/scripts/`).forEach((dossier) => {
        fs.readdirSync(`${publicFolder}/scripts/${dossier}/`).forEach((fichier) => {
          if(fichier.endsWith(".js") && !fichier.endsWith(".min.js")) {
            console.log(fichier);
            compresserFichierJS(`${publicFolder}/scripts/${dossier}/${fichier.slice(0, -3)}`);
          }
        });
      });
    } catch (err) {
      console.error(`Une erreur est survenu lors de la lecture des dossiers/fichiers à compresser "${err}"`);
    }

    fs.unlinkSync(cacheFileName);
    console.log("Les fichiers javascripts ont bien été compressés");
  });
}

function compresserTousFichiersCSS() {
  // Lecture de chaque fichier css du project
  try {
    fs.readdirSync(`${publicFolder}/styles/`).forEach((dossier) => {
      // console.log(dossier);
      if (dossier.endsWith(".min.css") || dossier.endsWith(".scss")) // Pas besoin de compresser le contenu du dossier admin puisque son exécution se fait en local
        return ;

      if (dossier.endsWith(".css")) {
        console.log(dossier);
        return compresserFichierCSS(dossier.slice(0, -4)); // Si c'est un fichier css directement dans le dossier styles
      }

      fs.readdirSync(`${publicFolder}/styles/${dossier}/`).forEach((fichier) => {
        if(fichier.endsWith(".css") && !fichier.endsWith(".min.css")) {
          console.log(fichier);
          compresserFichierCSS(`${dossier}/${fichier.slice(0, -4)}`);
        }
      });
    });
  } catch (err) {
    return console.error(`Une erreur est survenu lors de la lecture des dossiers/fichiers à compresser "${err}"`);
  }

  console.log("Les fichiers css ont bien été compressés");
}

compresserTousFichiersJS();
compresserTousFichiersCSS();
