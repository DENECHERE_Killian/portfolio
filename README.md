<div style="text-align: center;"><h1>Portfolio</h1></div>

https://killian-denechere.web.app/

## Sommaire

- [1. Présentation](#1-présentation)
- [2. Ajouter des projets](#2-ajouter-des-projets)
  - [2.1. Contenu fixe](#21-contenu-fixe)
  - [2.2. Contenu variable](#22-contenu-variable)
    - [2.2.1. Parties](#221-parties)
      - [2.2.1.1. Les conteneurs](#2211-les-conteneurs)
      - [2.2.1.2. Les contenus](#2212-les-contenus)
    - [2.2.2. Langages et Librairies](#222-langages-et-librairies)
    - [2.2.3. Ressources](#223-ressources)
    - [2.2.4. Liens](#224-liens)
    - [2.2.5. Images](#225-images)
- [3. Gérer les contenus textuels](#3-gérer-les-contenus-textuels)
  - [3.1. Traductions](#31-traductions)
  - [3.2. Portfolio](#32-portfolio)
  - [3.3. Conceptions](#33-conceptions)
  - [3.4. Erreurs](#34-erreurs)

## 1. Présentation
Comme le nom de celui-ci l'évoque assez bien, ce projet est un portfolio que j'ai un peu étendu de sorte à pouvoir le mettre à jour assez simplement.
Ayant pour objectif de me présenter au travers de quelques-uns de mes travaux, je tente aussi de mettre en avant différents aspects de ma personnalité et de mon évolution dans cette réalisation.

Je vous explique dans la suite du readme comment vous pouvez personnaliser votre portfolio si vous voulez prendre celui-ci comme base.

## 2. Ajouter des projets
Ajouter un projet au portfolio est simple : il suffit d'ajouter un fichier au format `json` dans le dossier [`/public/contents/projects/`](https://gitlab.com/DENECHERE_Killian/portfolio/-/tree/master/functions/public/contents/projects).

<span style="background-color: #df16384a">Un fichier de projet doit avoir une structure particulière qui est décrite en déail dans les parties ci-dessous. Vous pouvez vous servir du fichier [`base.json`](https://gitlab.com/DENECHERE_Killian/portfolio/-/blob/master/public/contents/projects/base.json) comme d'un exemple à partir duquel débuter.</span>


---
### 2.1. Contenu fixe

Un fichier json de projet possède certains champs qu'il est nécéssaire de renseigner :

- **`titre`** &nbsp; *(Chaine de caractères)* <br/>
  Le nom du projet. Il est utilisé pour désigner textuellement le projet.
- **`url`** &nbsp; *(Chaine de caractères)* <br/>
  Le lien dirigeant vers le fichier `json` du projet. Il faut que le nom de ce fichier soit le même que l'url choisi.
- **`imagePresentation`** &nbsp; *(Chaine de caractères)* <br/>
  Le nom de l'image principale du projet (utilisée pour représenter celui-ci). Ne pas oublier l'extension de l'image dans son nom. Les images sont à placer dans le dossier [`functions/public/medias/projects/`](https://gitlab.com/DENECHERE_Killian/portfolio/-/tree/master/functions/functions/public/medias/projects).
- **`descriptionPresentation`** &nbsp; *(Objet JSON)*
  - **`fr`** &nbsp; *(Chaine de caractères)* <br/>
    Une courte description du projet en français.
  - **`en`** &nbsp; *(Chaine de caractères)* <br/>
    Une courte description du projet en anglais.
- **`redirection`** &nbsp; *(Chaine de caractères)* <br/>
  Si la fiche de ce projet (disponible sur l'accueil du site) doit rediriger vers une autre page (celle du projet GitLab typiquement).
- **`details`** &nbsp; *(Objet JSON)*
  - **`dates`** &nbsp; *(Objet JSON)*
    - **`debut`** &nbsp; *(Chaine de caractères)* <br/>
      Date de début du projet sous forme `mois/année (ex: 15/20)`.
    - **`fin`** &nbsp; *(Chaine de caractères)* <br/>
      Date de fin du projet sous forme `mois/année (ex: 15/20)`.
  - **`contexte`** &nbsp; *(Objet JSON)*
    - **`personnel`** &nbsp; *(Booléen)* <br/>
      Si le projet a été réalisé dans le cadre personnel ou non.
    - **`scolaire`** &nbsp; *(Booléen)* <br/>
      Si le projet a été réalisé dans le cadre scolaire ou non.
    - **`nombreDePersonnes`** &nbsp; *(Number)* <br/>
      Le nombre de personnes ayant travaillé sur le projet.
- **`parties`** &nbsp; *(Liste d'objets JSON)* <br/>
  Voir la section "[parties](#221-parties)".
- **`conception`** &nbsp; *(Objet JSON)*
  - **`mode`** &nbsp; *(Chaine de caractères)* <br/>
    La valeur de ce champ doit être `conception` pour permettre au système d'identifier cette section de page comme étant un [conteneur spécial](#2211-les-conteneurs).
  - **`style`** &nbsp; *(Chaine de caractères)* <br/>
    Les règles de style css que vous souhaitez voir appliquer sur l'objet parent.
  - **`langages`** &nbsp; *(Liste d'objets JSON)* <br/>
    Voir la section "[langages et librairies](#222-langages-et-librairies)".
  - **`librairies`** &nbsp; *(Liste d'objets JSON)* <br/>
    Voir la section "[langages et librairies](#222-langages-et-librairies)".
  - **`ressources`** &nbsp; *(Liste d'objets JSON)* <br/>
    Voir la section "[ressources](#223-ressources)".
- **`liensExternes`** &nbsp; *(Liste d'objets JSON)* <br/>
  Voir la section "[liens](#224-liens)".


----
### 2.2. Contenu variable
#### 2.2.1. **Parties**

Les parties sont les différents liens qui sont affichés à la gauche de l'écran sur la page de détails d'un projet. À chaque partie est attribuée une zone d'affichage d'informations (la partie blanche centrale). Ainsi, afin de remplir cette dernière, vous pouvez créer des zones d'affichage en plaçant des zones : j'ai nommé  `conteneurs` et `contenus`. Leurs structures sont expliquées dans les parties ci-dessous.

<span style="background-color: #df16384a;">Il est possible de placer autant de contenus que voulu dans un conteneur (ex : conteneur[contenu, contenu, contenu]).</span>

<span style="background-color: #df16384a;">Seulement 2 niveaux imbriqués de conteneurs sont autorisés (ex : conteneur[contenu, contenu, conteneur[contenu, contenu], contenu]).</span>


----
##### 2.2.1.1. **Les conteneurs**

Pour créer sa propre mise en forme, il faut commencer par ajouter des conteneurs : ils ne sont pas visibles, mais servent à structurer l'affichage final. Chacun devra posséder au minimum les attributs listés ci-dessous :

- **`titre`** &nbsp; *(Objet JSON)*
  - **`fr`** &nbsp; *(Chaine de caractères)* <br/>
    Le titre de la partie en français.
  - **`en`** &nbsp; *(Chaine de caractères)* <br/>
    Le titre de la partie en anglais.
- **`description`** &nbsp; *(Objet JSON)*
  - **`fr`** &nbsp; *(Chaine de caractères)* <br/>
    Une explication en français pour la partie.
  - **`en`** &nbsp; *(Chaine de caractères)* <br/>
    Une explication en anglais pour la partie.
- **`mode`** &nbsp; *(Chaine de caractères)* <br/>
  Prend la valeur `vertical` **OU** `horizontal` et définit la direction dans laquelle vont être affichés les contenus.
- **`style`** &nbsp; *(Chaine de caractères)* <br/>
    Les règles de style css que vous souhaitez voir appliquer sur l'objet parent.
- **`sections`** &nbsp; *(Liste d'objets JSON)* &nbsp; La liste des contenus : ils peuvent être, soit un conteneur (dans la limite de 2 conteneurs imbriqués), soit un [contenu](#2212-les-contenus).


----
##### 2.2.1.2. **Les contenus**

Placés dans les [conteneurs](#2211-les-conteneurs), ce sont les éléments visibles (images, textes, carousel, image et texte à la fois). Chacun devra posséder au minimum les attributs listés ci-dessous :

- **`mode`** &nbsp; *(Chaine de caractères)* &nbsp; Définit le type de contenu affiché (valeur à choisir parmis celles disponibles dans le tableau ci-dessous).
- **`titre`** &nbsp; *(Objet JSON)*
  - **`fr`** &nbsp; *(Chaine de caractères)* &nbsp; Le titre en français de la partie.
  - **`en`** &nbsp; *(Chaine de caractères)* &nbsp; Le titre en anglais de la partie.
  - **`style`** &nbsp; *(Chaine de caractères)*<br/>
  Les règles de style css que vous souhaitez voir appliquer sur l'objet parent.
- **`style`** &nbsp; *(Chaine de caractères)*<br/>
  Les règles de style css que vous souhaitez voir appliquer sur l'objet parent.


Plusieurs contenus sont disponibles : certains nécéssitent de rajouter des champs à ceux listés ci-dessus.

| Mode         | Ajouts                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    | Description                                                                                                                                                                                                         |
|--------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| image        | Voir la section "[images](#225-images)".                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | Affiche une image seule avec, si complétée, une description affichée lors du zoom sur l'image (via le click).                                                                                                       |
| imageEtTexte | <ul><li>**`donnees`** &nbsp; *(Objet JSON)*<ul><li>**`image`** &nbsp; *(Chaine de caractères)*<ul>L'url (interne ou externe au projet) menant vers l'image.</ul></li><li>**`titre`** &nbsp; *(Objet JSON)*<ul>**Cet objet possède la même structure que le champ titre exposé au dessus du tableau.**</ul></li><li>**`texte`** &nbsp; *(Objet JSON)*<ul>**Cet objet possède la même structure que le champ titre exposé au dessus du tableau.**</ul></li><li>**`texteEnPremier`** &nbsp; *(Booléen = true)* &nbsp; Détermine la position du texte descriptif par rapport à l'image.</li><li>**`modeVignette`** &nbsp; *(Booléen = false)* &nbsp; Permet de choisir si l'image affichée seras laissée à sa taille d'origine (false) ou sera coupée au format carré (true) : laissant plus de place au texte.</li><li>**`style`** &nbsp; *(Chaine de caractères)*<ul>Les règles de style css que vous souhaitez voir appliquer sur l'objet parent.</ul></li></ul></li></ul> | Même affichage que la section `image` ci-dessus mis à part la présence d'un texte à côté de l'image : <br/> - gauche/droite de l'image : conteneur horizontal<br/> - dessus/dessous de l'image : conteneur vertical |
| carousel     | <ul><li>**`listeImages`** &nbsp; *(Liste d'objets JSON)*</li></ul>Voir la section "[images](#225-images)".                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | Affichage d'une liste d'images une par une.                                                                                                                                                                         |
| texte        | <ul><li>**`texte`** &nbsp; *(Objet JSON)*<ul><li>**`fr`** &nbsp; *(Chaine de caractères)* &nbsp; La version française du texte.</li><li>**`en`** &nbsp; *(Chaine de caractères)* &nbsp; La version anglaise du texte.</li></ul></li></ul>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | Affiche un simple paragraphe                                                                                                                                                                                        |


----
#### 2.2.2. **Langages et Librairies**

Lors de la description d'un projet, une section "conception" est présente et détaille les librairies, langages, etc. (aspects techniques) utilisés. Pour chaque outil utilisé, il est possible d'afficher un nom complet, un abrege ainsi qu'un ou plusieurs liens externes.

Pour éviter la redondance de ces informations, le fichier [`conceptions.json`](https://gitlab.com/DENECHERE_Killian/portfolio/-/blob/master/functions/public/contents/conceptions.json) les stocke pour nous ! Il suffit d'y renseigner la structure suivante pour chaque outil de votre choix et, dans le fichier json de votre projet, ne référencez que l'abrégé qui vous concerne dans la partie conception.

- **`abrege`** &nbsp; *(Chaine de caractères)* <br/>
  Le nom de l'outil sous forme abregée (ex : `js` pour JavaScript).
- **`liens`** &nbsp; *(Liste d'objets JSON)* <br/>
  Des liens supplémentaires à ajouter à la description de l'outil (documentation, site web ...). Tout en sachant qu'un lien principal `plus d'infos` est affiché en permanance.<br/>
  Voir la section "[liens](#224-liens)".
- **`utilite`** &nbsp; *(Objet JSON)*
  - **`fr`** &nbsp; *(Chaine de caractères)* <br/>
    L'explication en français relative à l'utilisation de l'outil au sein du projet.
  - **`en`** &nbsp; *(Chaine de caractères)* <br/>
    L'explication en anglais relative à l'utilisation de l'outil au sein du projet.


----
#### 2.2.3. **Ressources**

Toutes les `ressources` ont la même structure que les "[langages et librairies](#222-langages-et-librairies)" à laquelle il est rajouté un champ :

- **`attribution`** &nbsp; *(Chaine de caractères)* <br/>
  Un champ permettant l'affichage d'une attribution pour les potentielles sources le demandant.


----
#### 2.2.4. **Liens**

Tous les objets `lien` ont la structure suivante :

- **`titre`** &nbsp; *(Objet JSON)*
  - **`fr`** &nbsp; *(Chaine de caractères)* <br/>
    Le titre du lien en français.
  - **`en`** &nbsp; *(Chaine de caractères)* <br/>
    Le titre du lien en anglais.
- **`lien`** &nbsp; *(Chaine de caractères)* <br/>
  L'url du lien.


----
#### 2.2.5. **Images**

Tous les objets `image` ont la structure suivante :

- **`lien`** &nbsp; *(Chaine de caractères)* <br/>
  L'url (interne ou externe au projet) menant vers l'image.
- **`commentaire`** &nbsp; *(Objet JSON)*
  - **`fr`** &nbsp; *(Chaine de caractères)* <br/>
    Un texte descriptif en français affiché au zoom sur l'image (via le click sur l'image).
  - **`en`** &nbsp; *(Chaine de caractères)* <br/>
    Un texte descriptif en anglais affiché au zoom sur l'image (via le click sur l'image).

## 3. Gérer les contenus textuels
### 3.1. **Traductions**
Le site web contient beaucoup de textes différents, chacun devant être traduit dans plusieurs langues.

Pour cela, les textes statiques du site sont traduits par l'intermédiaire d'un module qui charge les textes correspondant à la langue actuelle du site (en fonction d'un cookie).
Toutes ces traductions se situent dans le dossier [`public/content/languages/`](https://gitlab.com/DENECHERE_Killian/portfolio/-/tree/master/functions/public/contents/languages) sous forme de fichier `.json`.
Il suffit donc de rajouter un fichier json pour rajouter une langue disponible sur le site.

En ce qui concerne les informations des projets, des expériences, ... et plus globalement, de tout le contenu ajouté au fur et à mesure du temps, les traductions des textes se situent dans les fichiers `.json` stockant les projets, expériences, etc.

----
### 3.2. **Portfolio**
Parmis toutes les informations traduites, il y en a certaines qui sont directement reliées au portfolio : mon nom et mon prénom par exemple. Pour séparer ce type d'informations de celles plus "globale" (le titre projet de la page principale par exemple), j'ai créé le fichier [`portfolio.json`](https://gitlab.com/DENECHERE_Killian/portfolio/-/blob/master/functions/public/contents/portfolio.json).

----
### 3.3. **Conceptions**
Lorsque l'on se situe sur la page de détails d'un projet, il y a une partie à gauche de l'écran qui est toujours affiché : la partie "conception". Elle sert à afficher tous les outils que j'ai utilisés pour réaliser le projet en question (librairies, paquet, langage, ...). Pour éviter de dupliquer les informations de chacun de ces outils (qui peuvent être réutilisés d'un projet à un autre), je peux les renseigner dans le fichier [`conceptions.json`](https://gitlab.com/DENECHERE_Killian/portfolio/-/blob/master/public/contents/conceptions.json) et les afficher une fois sur la page de détails du projet.

**D'autres informations sont disponibles dans la partie "[langages et librairies](#222-langages-et-librairies)".**

----
### 3.4. **Erreurs**
Concevoir un fichier JSON de projet n'est pas une chose simple : une erreur est vite survenue malgré les efforts apportés à la conception de la documentation. Aussi, pour aider un peu plus à la conception/correction de ces fichiers, j'ai créé un système d'erreurs qui indiquent ce qui manque ou est mal écrit dans un fichier (attention, toutes les erreurs ne sont sûrement pas prises en compte).

Retrouvez la liste des erreurs dans le fichier "[`erreurs.json`](https://gitlab.com/DENECHERE_Killian/portfolio/-/blob/master/functions/public/contents/errors.json)".
